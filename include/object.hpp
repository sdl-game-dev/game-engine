//
//  object.hpp
//  object class for GameEngine
//  This class will be template for game developers to overload
//
//  Created by Christopher Mahon 25/9/17.
//  Copyright © 2017 Christopher. All rights reserved.
//

#pragma once

#include <stdio.h>
#include <math.h>
#include <vector>
#include <iostream>
#include <string>
#include <typeinfo>

#include "vector2.hpp"
#include "component.hpp"

class Engine;

class Object {
friend class Engine;
 public:
    Object();
    explicit Object(std::string);
    Object(std::string, Vector2*);
    ~Object();

    std::string objName;

    void update();
    void draw();

    Vector2 position(void);
    void move(Vector2);
    bool isActive() { return this->active; }
    void destroy() { this->active = false; }

    bool addComponent(Component*);
    void deleteComponent();

    template<class T> Component* getComponent(){
        std::string classType = typeid(T).name();
        std::cout << "Beginning search with " << classType << std::endl;
        for (Component* component: this->components)
            if (component) {
                std::cout << "Found " << component->type << std::endl;
                if (classType == component->type)
                    return component;
            }
        return nullptr;
    }

 protected:
    std::string spriteName;

 private:
    Vector2 *coords;
    bool active = false;
    std::vector<Component *> components;
};
