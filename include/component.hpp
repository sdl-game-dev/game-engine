//
//  component.hpp
//  component class that will be used as a basis for all
//      other components
//  Used https://www.youtube.com/watch?v=XsvI8Sng6dk as
//      initial starting point
//
//  Created by Christopher Mahon 7/11/17.
//  Copyright © 2017 Christopher. All rights reserved.
//

#pragma once
#include <iostream>
#include <string>
#include <typeinfo>

#include "vector2.hpp"
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"

class Object;

class Component {
 public:
    Component() {}
    explicit Component(Object*);
    virtual void update() = 0;
    virtual void draw() = 0;
    virtual ~Component() {}
    std::string type;

 protected:
    Object *m_pEntity;
};

class KeyboardMovementComponent: public Component {
 public:
    explicit KeyboardMovementComponent(Object *);
    void update();
    void draw();
};

class SpriteDrawComponent: public Component {
 public:
    // Surface for the sprite
    SDL_Texture* sprite = NULL;
    SDL_Rect rect_source, rect_dest;
    int max_width, max_height, width, height, speed;
    int frame_counter = 0;
    int scale = 1;
    SpriteDrawComponent(Object *, std::string, int, int, int, int);
    ~SpriteDrawComponent();
    void update();
    void draw();
};
