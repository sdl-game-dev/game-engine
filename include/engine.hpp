//
//  engine.hpp
//  Engine class for GameEngine
//
//  Created by Christopher Mahon 28/11/18.
//  Copyright © 2018 Christopher. All rights reserved.
//

#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <vector>

#include "SDL2/SDL.h"

#include "controls.hpp"
#include "object.hpp"

#define STEP 4

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int FPS = 60;
const float frameDelay = 1000 / FPS;

class Engine {
 public:
    Engine();
    ~Engine();
    static bool registerme(Object *obj);
    static bool unregister(Object *obj);
    static int init();
    static int sdl_init();
    static bool update();
    static int destroy();

    static Controls* controller() {return Engine::control;}

    static bool isRunning;
    static std::vector<Object *> entities;

    static SDL_Surface *screenSurface;
    static SDL_Renderer *renderer;

 protected:
    static bool draw();
    static Controls* control;
    static SDL_Window *window;
};
