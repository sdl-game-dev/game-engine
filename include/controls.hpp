//
//  controls.hpp
//  Controls class for GameEngine
//  This class will be responsible for interfacing with keyboardfor key input
//
//  Created by Christopher Mahon 7/11/17.
//  Copyright © 2017 Christopher. All rights reserved.
//

#pragma once
#include <SDL2/SDL.h>
#include <stdio.h>

// To decide keys for A, B, X, Y, Start
#define KEY_A SDLK_z
#define JOY_A SDL_CONTROLLER_BUTTON_A
#define KEY_B SDLK_x
#define JOY_B SDL_CONTROLLER_BUTTON_B
#define KEY_X SDLK_a
#define JOY_X SDL_CONTROLLER_BUTTON_X
#define KEY_Y SDLK_s
#define JOY_Y SDL_CONTROLLER_BUTTON_Y
#define KEY_START SDLK_RETURN
#define JOY_START SDL_CONTROLLER_BUTTON_START
#define KEY_UP SDLK_UP
#define JOY_UP SDL_CONTROLLER_BUTTON_DPAD_UP
#define KEY_DOWN SDLK_DOWN
#define JOY_DOWN SDL_CONTROLLER_BUTTON_DPAD_DOWN
#define KEY_LEFT SDLK_LEFT
#define JOY_LEFT SDL_CONTROLLER_BUTTON_DPAD_LEFT
#define KEY_RIGHT SDLK_RIGHT
#define JOY_RIGHT SDL_CONTROLLER_BUTTON_DPAD_RIGHT
#define KEY_QUIT SDLK_ESCAPE
#define JOY_QUIT SDL_CONTROLLER_BUTTON_BACK

class Controls {
 public:
    Controls();
    ~Controls();

    bool a, b, x, y, start;
    bool left, right, up, down;
    bool quit;
    int update();

 protected:
    int reset();
};
