//
//  vector2.cpp
//  Vector2 class for GameEngine
//  This class will be for holding all xy co-ordinates
//  May eventually get some geometry functions in here
//
//  Created by Christopher Mahon 28/6/2016.
//  Copyright © 2016 Christopher Mahon. All rights reserved.
//

#include "../include/vector2.hpp"
#include <math.h>
#include <iostream>

Vector2::Vector2(double tX, double tY): x(tX), y(tY) {
}

Vector2::Vector2(int tX, int tY): x(tX), y(tY) {
}

Vector2::Vector2(Vector2 *vect) {
    this->x = vect->x;
    this->y = vect->y;
}

Vector2::Vector2() {
    this->x = this->y = 0;
}

Vector2::~Vector2() {
    // do nothing
}

double Vector2::distance(Vector2 vect2) {
    float totalX, totalY, combination;
    totalX = pow(vect2.x - this->x, 2);
    totalY = pow(vect2.y - this->y, 2);
    combination = totalX + totalY;
    double dist = sqrt(combination);
    return dist;
}

double Vector2::diff_x(Vector2 vect2) {
    double difference;
    difference = this->x - vect2.x;
    return difference;
}

double Vector2::diff_y(Vector2 vect2) {
    double difference;
    difference = this->y - vect2.y;
    return difference;
}

Vector2 Vector2::operator+(Vector2 other) {
    double totX = this->x + other.x;
    double totY = this->y + other.y;
    return Vector2(totX, totY);
}

Vector2 Vector2::operator+(int adder) {
    double totX = this->x + adder;
    double totY = this->y + adder;
    return Vector2(totX, totY);
}

Vector2 Vector2::operator+(float adder) {
    double totX = this->x + adder;
    double totY = this->y + adder;
    return Vector2(totX, totY);
}

void Vector2::operator+=(Vector2 other) {
    std::cout << this->x << std::endl;
    std::cout << other.x << std::endl;
    this->x += other.x;
    this->y += other.y;
}

void Vector2::operator+=(int adder) {
    this->x = this->x + adder;
    this->y = this->y + adder;
}

void Vector2::operator+=(float adder) {
    this->x = this->x + adder;
    this->y = this->y + adder;
}

Vector2 Vector2::operator-(Vector2 other) {
    double totX = this->x - other.x;
    double totY = this->y - other.y;
    return Vector2(totX, totY);
}

Vector2 Vector2::operator-(int adder) {
    double totX = this->x - adder;
    double totY = this->y - adder;
    return Vector2(totX, totY);
}

Vector2 Vector2::operator-(float adder) {
    double totX = this->x - adder;
    double totY = this->y - adder;
    return Vector2(totX, totY);
}

Vector2 Vector2::operator*(Vector2 other) {
    double totX = this->x * other.x;
    double totY = this->y * other.y;
    return Vector2(totX, totY);
}

Vector2 Vector2::operator*(int adder) {
    double totX = this->x * adder;
    double totY = this->y * adder;
    return Vector2(totX, totY);
}

Vector2 Vector2::operator*(float adder) {
    double totX = this->x * adder;
    double totY = this->y * adder;
    return Vector2(totX, totY);
}

Vector2 Vector2::operator/(Vector2 other) {
    double totX = this->x / other.x;
    double totY = this->y / other.y;
    return Vector2(totX, totY);
}

Vector2 Vector2::operator/(int adder) {
    double totX = this->x / adder;
    double totY = this->y / adder;
    return Vector2(totX, totY);
}

Vector2 Vector2::operator/(float adder) {
    double totX = this->x / adder;
    double totY = this->y / adder;
    return Vector2(totX, totY);
}


void Vector2::set_values(double x, double y) {
    this->x = x;
    this->y = y;
}

void Vector2::set_values(Vector2 newVals) {
    this->x = newVals.x;
    this->y = newVals.y;
}
