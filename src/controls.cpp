//
//  controls.cpp
//  Controls class for GameEngine
//  This class will be responsible for interfacing with keyboard for key input
//
//  Created by Christopher Mahon 7/11/17.
//  Copyright © 2017 Christopher. All rights reserved.
//

#include "../include/controls.hpp"

Controls::Controls() {
    quit = false;
    reset();
}

Controls::~Controls() {
    // do nothing
}

int Controls::update() {
    SDL_Event e;
    while (SDL_PollEvent(&e) != 0) {
        if (e.type == SDL_QUIT) {
            quit = true;
            return 0;
        } else if (e.type == SDL_KEYDOWN) {
            // User has pressed a key
            // Select surfaces based on key press
            switch (e.key.keysym.sym) {
                case KEY_QUIT:
                    quit = true;
                    break;

                case KEY_UP:
                    up = true;
                    break;

                case KEY_DOWN:
                    down = true;
                    break;

                case KEY_LEFT:
                    left = true;
                    break;

                case KEY_RIGHT:
                    right = true;
                    break;

                case KEY_A:
                    a = true;
                    break;

                case KEY_B:
                    b = true;
                    break;

                case KEY_X:
                    x = true;
                    break;

                case KEY_Y:
                    y = true;
                    break;

                case KEY_START:
                    start = true;
                    break;

                default:
                    break;
            }
        } else if (e.type == SDL_KEYUP) {
            // User has released a key
            // Select surfaces based on key press
            switch (e.key.keysym.sym) {
                case KEY_QUIT:
                    quit = true;
                    break;

                case KEY_UP:
                    up = false;
                    break;

                case KEY_DOWN:
                    down = false;
                    break;

                case KEY_LEFT:
                    left = false;
                    break;

                case KEY_RIGHT:
                    right = false;
                    break;

                case KEY_A:
                    a = false;
                    break;

                case KEY_B:
                    b = false;
                    break;

                case KEY_X:
                    x = false;
                    break;

                case KEY_Y:
                    y = false;
                    break;

                case KEY_START:
                    start = false;
                    break;

                default:
                    break;
            }
        }
    }
    // Temporarily being left in to ensure keys are working as intended
    printf("DEBUG (%d, %d, %d, %d, %d, %d, %d, %d, %d)\n",
        a, b, x, y, start, left, right, up, down);
    return 0;
}

int Controls::reset() {
    a = false;
    b = false;
    x = false;
    y = false;
    start = false;
    left = false;
    right = false;
    up = false;
    down = false;

    return 0;
}
