//
//  object.cpp
//  object class for GameEngine
//  This class will be the object that components are attached to
//  This will allow things happen
//
//  Created by Christopher Mahon 25/9/17.
//  Copyright © 2017 Christopher. All rights reserved.
//


#include "../include/object.hpp"
#include "../include/engine.hpp"

Object::Object():objName("Name") {
    coords = new Vector2(0, 0);
    Engine::registerme(this);
}

Object::Object(std::string name):objName(name), active(true) {
    coords = new Vector2(6, 9);
    Engine::registerme(this);
}

Object::Object(std::string name, Vector2 *location):
    objName(name), coords(location), active(true) {
    Engine::registerme(this);
}

Object::~Object() {
    // do nothing
}


void Object::update() {
    for (Component* component : this->components)
        if (component)
            component->update();
}

void Object::draw() {
    for (Component* component : this->components)
        if (component)
            component->draw();
}

Vector2 Object::position() {
    return *(this->coords);
}

void Object::move(Vector2 move_dir) {
    this->coords->set_values(*this->coords + move_dir);
}

bool Object::addComponent(Component *component) {
    this->components.push_back(component);
    return true;
}
