//
//  component.cpp
//  Implementation of various functions of built in function
//
//  Created by Christopher Mahon 7/12/18.
//  Copyright © 2018 Christopher. All rights reserved.
//

#include "../include/component.hpp"
#include "../include/engine.hpp"

Component::Component(Object *pObj):m_pEntity(pObj) {
    this->m_pEntity->addComponent(this);
}

KeyboardMovementComponent::KeyboardMovementComponent(
    Object *pObj):Component(pObj) {
    this->type = typeid(*this).name();
}

void KeyboardMovementComponent::update() {
    int x, y;
    x = y = 0;
    if (Engine::controller()->up == 1)
        y -= (1*STEP);
    if (Engine::controller()->down == 1)
        y += (1*STEP);
    if (Engine::controller()->left == 1)
        x -= (1*STEP);
    if (Engine::controller()->right == 1)
        x += (1*STEP);
    this->m_pEntity->move(Vector2(x, y));
    return;
}

void KeyboardMovementComponent::draw() {
    return;
}

SpriteDrawComponent::SpriteDrawComponent(
    Object *pObj, std::string image,
    int width, int height, int speed, int scale):
        Component(pObj), width(width),
        height(height), speed(speed), scale(scale) {
    this->type = typeid(*this).name();
    SDL_Surface *ptmpSurface = IMG_Load(image.c_str());
    if (ptmpSurface == NULL) {
        printf("Unable to load image %s! SDL Error: %s\n",
            image.c_str(), IMG_GetError());
    }
    this->sprite = SDL_CreateTextureFromSurface(Engine::renderer, ptmpSurface);
    if (this->sprite == NULL) {
        printf("Unable to load image %s! SDL Error: %s\n",
            image.c_str(), SDL_GetError());
    }
    SDL_QueryTexture(this->sprite, NULL, NULL,
        &this->rect_source.w, &this->rect_source.h);
    this->rect_source.x = 0;
    this->rect_source.y = 0;
    this->max_width = this->rect_source.w;
    this->max_height = this->rect_source.h;
}

SpriteDrawComponent::~SpriteDrawComponent() {
    SDL_DestroyTexture(this->sprite);
    this->sprite = NULL;
}

void SpriteDrawComponent::update() {
    return;
}

void SpriteDrawComponent::draw() {
    int FRAME = 5;
    this->rect_dest.x = this->m_pEntity->position().x;
    this->rect_dest.y = this->m_pEntity->position().y;
    this->rect_dest.w = this->rect_source.w*this->scale;
    this->rect_dest.h = this->rect_source.h*this->scale;

    this->rect_source.w = this->width;
    this->rect_source.h = this->height;

    this->frame_counter = (this->frame_counter+1) % this->speed;

    if (this->frame_counter == 0) {
        this->rect_source.x =
            (this->rect_source.x+this->width) % this->max_width;
        this->rect_source.y = this->rect_source.y;
    }
    SDL_RenderCopy(Engine::renderer, this->sprite,
        &this->rect_source, &this->rect_dest);
    return;
}
