//
//  main.cpp
//
//  Created by Christopher Mahon 25/9/17.
//  Copyright © 2017 Christopher. All rights reserved.
//

#include "../include/main.h"

int main(int argc, char *argv[]) {
    try {
        Engine::init();
        Object *pObject = new Object("Fred", new Vector2(5*STEP, 3*STEP));
        Component *pMoveComponent = new KeyboardMovementComponent(pObject);
        Component *pDrawComponent = new SpriteDrawComponent(
            pObject, "assets/slime.png", 16, 16, 6, 5);

        Object *obj2 = new Object("Bush", new Vector2(16*STEP, 64*STEP));
        new SpriteDrawComponent(obj2, "assets/bush.png", 16, 16, 1, 4);

        Object *obj3 = new Object("Slime", new Vector2(30*STEP, 45*STEP));
        new SpriteDrawComponent(obj3, "assets/slime.png", 16, 16, 5, 6);

        Object *obj4 = new Object("Projectile", new Vector2(7*STEP, 45*STEP));
        new SpriteDrawComponent(obj4, "assets/projectile.png", 16, 16, 3, 6);

        Object *obj5 = new Object("Slime2", new Vector2(50*STEP, 45*STEP));
        new SpriteDrawComponent(obj5, "assets/slime.png", 16, 16, 6, 6);

        Component* comp1 = pObject->getComponent<SpriteDrawComponent>();
        Component* comp2 = pObject->getComponent<KeyboardMovementComponent>();

        if (comp1 != nullptr)
            std::cout << comp1->type << std::endl;
        else
            std::cout << "There is no Sprite Draw" << std::endl;

        if (comp2 != nullptr)
            std::cout << comp2->type << std::endl;
        else
            std::cout << "There is no Keyboard Move" << std::endl;

        while (Engine::isRunning)
            Engine::update();

        Engine::destroy();
    }
    catch(std::exception& e) {
        std::cout << "EXCEPTION: BZZZZZT ERROR\n";
        std::cout << e.what() << std::endl;
        return 1;
    }
    return 0;
}
